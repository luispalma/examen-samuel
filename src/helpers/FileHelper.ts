import fs from 'fs'

export default class FileHelper {
  /**
   * Read a file
   *
   * @param path The path of the file
   * @param encode The file encode
   *
   * @throws Error
   * @returns The file string content
   */
  public static async read(path: string, encode = 'utf8'): Promise<string> {
    const promise: Promise<string> = new Promise((resolve, reject) => {
      fs.readFile(path, encode, (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })

    try {
      const data = await promise
      return data
    } catch (error) {
      throw error
    }
  }
}
