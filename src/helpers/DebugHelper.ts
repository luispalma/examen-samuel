import Debug from 'debug'

export default class DebugHelper {
  private static _debug: Debug.IDebugger

  /**
   * Debug Singleton
   *
   * @returns The debug instance
   */
  public static getDebug(): Debug.IDebugger {
    if (!DebugHelper._debug) {
      DebugHelper._debug = Debug('console:command')
    }

    return DebugHelper._debug
  }

  /**
   * Debug Log message
   *
   * @param formatter The message with format
   * @param args The format args
   */
  public static log(formatter: any, ...args: any[]): void {
    const debug = DebugHelper.getDebug()

    debug(formatter, ...args)
  }
}
