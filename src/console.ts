/* eslint-disable import/first */
import program from 'commander';

import appModulePath from 'app-module-path';
appModulePath.addPath(__dirname, null);

import packageJson from '../package.json';

import Task1Command from 'commands/Task1Command';
import Task2Command from 'commands/Task2Command';
import Task3Command from 'commands/Task3Command';

program.version(packageJson.version);

program
  .command('task1 <x1> <y1> <x2> <y2> <x3> <y3> <x4> <y4>')
  .description('Task #1')
  .action(async (x1, y1, x2, y2, x3, y3, x4, y4) => {
    await new Task1Command(x1, y1, x2, y2, x3, y3, x4, y4).run();
  });

program
  .command('task2')
  .description('Task #2')
  .action(async () => {
    await new Task2Command().run();
  });

program
  .command('task3')
  .description('Task #3')
  .action(async () => {
    await new Task3Command().run();
  });

program.parse(process.argv);
export default program;
