import path from 'path';
import validator from 'validator';

import Command from 'commands/Command';
import DebugHelper from 'helpers/DebugHelper';
import FileHelper from 'helpers/FileHelper';

export default class Task2Command extends Command {
  _data: number[][] = [];

  constructor() {
    super();
    DebugHelper.log('Task2Command');
  }

  async run(): Promise<void> {
    try {
      this._validate();
      await this._getData();

      const baseNumber = this._getBaseNumber();
      console.log(`Base Number: ${baseNumber}`);
    } catch (error) {
      DebugHelper.log('%O', error);
      process.exit(1);
    }
  }

  /**
   * Validate parameters
   */
  private _validate() {}

  private async _getData() {
    const data = await FileHelper.read(
      path.join(__dirname, '../..', 'assets/piramide.txt')
    );

    const rows = data.split('\n');
    for (let i = 0; i < rows.length; i++) {
      const columns = rows[i].split(' ');
      if (columns.length <= 1 && columns[0].trim().length <= 0) continue;

      const dataColumn: number[] = [];
      for (let j = 0; j < columns.length; j++) {
        dataColumn.push(parseInt(columns[j]));
      }
      this._data.push(dataColumn);
    }

    DebugHelper.log('%O', this._data);
  }

  private _getBaseNumber() {
    let total = 0;
    let lastColumn = 0;

    for (let i = 0; i < this._data.length; i++) {
      lastColumn = this._getMaxAdjacentColumn(i, lastColumn);
      total += this._data[i][lastColumn];
    }

    return total;
  }

  private _getMaxAdjacentColumn(row: number, lastColumn: number) {
    if (row > 0) {
      if (this._data[row][lastColumn] >= this._data[row][lastColumn + 1]) {
        return lastColumn;
      }
      return lastColumn + 1;
    }

    return 0;
  }
}
