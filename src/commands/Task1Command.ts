import validator from 'validator';

import Command from 'commands/Command';
import DebugHelper from 'helpers/DebugHelper';
import Point from 'models/point';

export default class Task1Command extends Command {
  _points: Point[] = [];

  constructor(
    x1: number,
    y1: number,
    x2: number,
    y2: number,
    x3: number,
    y3: number,
    x4: number,
    y4: number
  ) {
    super();
    this._points.push({ x: x1, y: y1 });
    this._points.push({ x: x2, y: y2 });
    this._points.push({ x: x3, y: y3 });
    this._points.push({ x: x4, y: y4 });

    DebugHelper.log('Task1Command');
    DebugHelper.log('Points', this._points);
  }

  async run(): Promise<void> {
    try {
      this._validate();

      if (this._isSquare()) {
        console.log('Is Square');
      } else {
        console.log('Not Square');
      }
    } catch (error) {
      DebugHelper.log('%O', error);
      process.exit(1);
    }
  }

  /**
   * Validate parameters
   */
  private _validate() {
    // avoid point
    if (
      this._points[0].x === this._points[1].x &&
      this._points[0].x === this._points[2].x &&
      this._points[0].x === this._points[3].x
    ) {
      throw new Error('Invalid. Not point in X')
    }

    if (
      this._points[0].y === this._points[1].y &&
      this._points[0].y === this._points[2].y &&
      this._points[0].y === this._points[3].y
    ) {
      throw new Error('Invalid. Not point in Y')
    }
  }

  private _isSquare() {
    const posX = this._existOtherSameX(this._points[0].x, 0);
    if (posX === false) return false;
    switch (posX) {
      case 1:
        if (this._points[2].x !== this._points[3].x) {
          return false;
        }
        break;
      case 2:
        if (this._points[1].x !== this._points[3].x) {
          return false;
        }
        break;
      case 3:
        if (this._points[1].x !== this._points[2].x) {
          return false;
        }
        break;
    }

    const posY = this._existOtherSameY(this._points[0].y, 0);
    if (posY === false) return false;
    switch (posY) {
      case 1:
        if (this._points[2].y !== this._points[3].y) {
          return false;
        }
        break;
      case 2:
        if (this._points[1].y !== this._points[3].y) {
          return false;
        }
        break;
      case 3:
        if (this._points[1].y !== this._points[2].y) {
          return false;
        }
        break;
    }

    return true;
  }

  private _existOtherSameX(x: number, exclude: number): number | boolean {
    for (let i = 0; i < this._points.length; i++) {
      if (i === exclude) continue;
      if (this._points[i].x === x) return i;
    }
    return false;
  }

  private _existOtherSameY(y: number, exclude: number): number | boolean {
    for (let i = 0; i < this._points.length; i++) {
      if (i === exclude) continue;
      if (this._points[i].y === y) return i;
    }
    return false;
  }
}
