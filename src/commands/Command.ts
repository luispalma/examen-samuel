import DebugHelper from 'helpers/DebugHelper'

export default class Command {
  constructor() {
  }

  /**
   * Runs the command
   */
  public async run(): Promise<void> {
    try {
      throw new Error('Missing implementation of run.')
    } catch (error) {
      DebugHelper.log('%O', error)
      process.exit(1)
    }
  }
}
