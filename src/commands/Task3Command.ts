import validator from 'validator';

import Command from 'commands/Command';
import DebugHelper from 'helpers/DebugHelper';

export default class Task3Command extends Command {
  constructor() {
    super();
    DebugHelper.log('Task3Command');
  }

  async run(): Promise<void> {
    try {
      this._validate();

      for (let i = 0; i < 10000; i++) {
        const macheteNumber = this._isMacheteNumber(i);
        if (macheteNumber === true) {
          console.log(`${i} is machete number`);
        } else {
          // console.log(`${i} is not machete number (${macheteNumber})`);
        }
      }
    } catch (error) {
      DebugHelper.log('%O', error);
      process.exit(1);
    }
  }

  /**
   * Validate parameters
   */
  private _validate() {}

  private _isMacheteNumber(val:number) {
    let base = val
    for (let i=0; i<50; i++) {
      const inverse = this._reverseNumber(base)
      base = base + inverse
      const palindrome = this._isPalindrome(base)
      if (palindrome) return base
    }

    return true;
  }

  private _reverseNumber(val: number) {
    return parseInt(val.toString().split('').reverse().join(''))
  }

  private _isPalindrome(val: number) {
    if (val === this._reverseNumber(val)) return true
    return false
  }
}
