# Resolución de problemas
Prueba diseñada por Samuel Chávez

## Instrucciones
Esta evaluación tiene como propósito medir su capacidad de resolución de situaciones complejas, aplicando lógica, creatividad e ingenio. Se presentan varios tasks, trate de resolver la mayor cantidad posible antes que se agote el tiempo. Ojo: un problema resuelto perfectamente será mejor ponderado que varios a medias.

## Task 1: Cuadrados
Escriba un programa que resuelva el siguiente problema: dadas cuatro tuplas (en ningún orden en particular) representando cuatro puntos en un espacio bidimensional, determine si en conjunto representan un cuadrado.

## Task 2: Pirámides de Egipto
En los antiguos manuscritos egipcios se encontraban series de números en formas peculiares. Uno de los más intrigantes casos eran las distribuciones en forma de pirámide.

Considere la siguiente representación traducida y simplificada de una de ellas:

         1
        8 6
       9 7 5
      0 2 9 4
     7 1 9 6 3
        32

En la base de estas pirámides, los arqueólogos siempre encontraban un entero grande. Para este ejemplo el entero encontrado fue 32. 

Luego de analizar varios casos, notaron que el valor estaba completamente determinado por la estructura de la pirámide de enteros que lo precedía. En general, el número en la base se encuentra a través de calcular la suma máxima del camino de enteros adyacentes.

Si definimos la adyacencia de manera similar como se haría con un árbol binario, tendríamos que los adyacentes serían (por su orden natural según la lectura):

Adyacentes de 1: 8, 6
Adyacentes de 8: 9 y 7
Adyacentes de 6: 7 y 5
Adyacentes de 9: 0, 2
Adyacentes de 7: 2, 9
Adyacentes de 5: 6, 4
Etcétera...

Entonces, el camino de adyacentes con la máxima suma sería 1 + 6 + 7 + 9 + 9 = 32

Descargue el archivo piramide.txt, que contiene una pirámide con 100 filas. Luego calcule el  entero que debería aparecer en su base.

## Task 3: Números Machete
Considere el número entero 47

Si al 47 le sumamos su reflejo (74) obtenemos 121, un número palíndromo (es decir, un número que se lee igual al derecho que al revés). Este proceso no concluye de la misma manera  con todos los números. Tome por ejemplo el entero 349:

349 + 943 = 1292
1292 + 2921 = 4213
4213 + 3124 = 7337

Se cree que algunos números jamás producen un palíndromo al aplicar este proceso. A estos números les llamaremos Números Machete. 

Un buen estimador para determinar si un número es Machete o no, es verificar si este produce un palíndromo en 50 iteraciones o menos. Si el número no produce un palíndromo en esta cantidad de iteraciones como máximo, lo consideraremos un Número Machete.

Usando el anterior estimador, calcule cuántos Números Machete hay menores que 10,000.
